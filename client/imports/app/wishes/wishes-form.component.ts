import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Wishes } from '../../../../both/collections/wishes.collection';

import template from './wishes-form.component.html';

@Component({
    selector: 'wishes-form',
    template
})

export class WishesFormComponent implements OnInit {
    abstract;
    addForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.addForm = this.formBuilder.group({
            wish: ['', Validators.required]
        });
    }

    addWish(): void {
        if (this.addForm.valid) {
            Wishes.insert(this.addForm.value);

            //this.addForm.reset();
        }
    }
}