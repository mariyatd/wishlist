import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Wishes } from '../../../both/collections/wishes.collection';
import { Wish } from '../../../both/models/wish.model';
import template from './app.component.html';

@Component({
    selector: 'app',
    template
})
export class AppComponent {
    wishes: Observable<Wish[]>;

    constructor() {
        this.wishes = Wishes.find({}).zone();
    }
    removeWish(wish: Wish): void {
        Wishes.remove(wish._id);
    }
}