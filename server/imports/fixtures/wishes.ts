import { Wishes } from '../../../both/collections/wishes.collection'
import { Wish } from '../../../both/models/wish.model';


export function loadWishes() {
    if (Wishes.find().cursor.count() === 0) {
        const wishes: Wish[] = [{
            wish: 'Pen'
        }, {
            wish: 'Pineapple'
        }, {
            wish: 'Apple-pen'
        }];

        wishes.forEach((wish: Wish) => Wishes.insert(wish));
    }
}