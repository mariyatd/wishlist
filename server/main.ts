import { Meteor } from 'meteor/meteor';

import { loadWishes } from './imports/fixtures/wishes';

Meteor.startup(() => {
    loadWishes();
});